var gulp = require('gulp');
var compass = require( 'gulp-compass' );
var notify = require( 'gulp-notify' );
var reload = require( 'gulp-livereload' );
var connect = require('gulp-connect');
var wiredep = require('wiredep').stream;
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var clean = require('gulp-clean');
var sftp = require('gulp-sftp');
var rigger = require('gulp-rigger'); // подключение частей шаблона по типу //=template/footer.html
var sourcemaps = require('gulp-sourcemaps');

/*
************* SERVER **************
*/
gulp.task('connect', function() {
  connect.server({
    root: 'dev',
    port: 8000,
    livereload: true
  });
});


/*
************* VENDOR LIBRARYS **************
*/
gulp.task('bower', function () {
  gulp.src('./dev/html/index.html')
    .pipe(sourcemaps.init())
    .pipe(wiredep({
      directory : "dev/bower_components"
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dev/html/'));
});


/*
************* COMPASS **************
*/
gulp.task( 'compass', function(){
    gulp.src('dev/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(compass({
            config_file: 'dev/config.rb',
            css: 'dev/css',
            sass: 'dev/sass'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dev/css'))
        .pipe(connect.reload())
        .pipe(notify('Done!'));
});


/*
************* HTML **************
*/
gulp.task('html', ['clean'], function(){
    gulp.src('dev/html/template/*.html')
    .pipe(rigger())
    .pipe(gulp.dest('dev/html/'))
    .pipe(connect.reload());
});

/*
************* js **************
*/
gulp.task('js', function(){
    gulp.src('dev/js/template/*.js')
    .pipe(rigger())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dev/js/'))
    .pipe(connect.reload());
});


/*
************* WATCH **************
*/
gulp.task('watch', function(){
    gulp.watch('dev/sass/*.scss', ['compass']);
    gulp.watch('dev/html/*/**', ['html']);
    gulp.watch('dev/js/main.html', ['js']);
    gulp.watch('bower.json', ['bower']);

});


/*
************* DEV PROJECT **************
*/
gulp.task('default', ['connect', 'compass', 'js', 'html', 'watch', 'bower']);






/*
************* clean **************
*/
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});


/*
************* project ready **************
*/
gulp.task('project-build', ['clean'], function () {
    return gulp.src('dev/html/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('dist'));
});


/*
************* project test **************
*/
gulp.task('project-test', function() {
  connect.server({
    root: 'dist',
    port: 8000,
    livereload: true
  });
});

/*
************* project ftp **************
*/
gulp.task('sftp', function () {
    return gulp.src('dist/**/*')
        .pipe(sftp({
            host: 'website.com',
            user: 'johndoe',
            pass: '1234',
            removePatch: ''//точный адрес домашней папки -- могут подсказать в тп
        }));
});

